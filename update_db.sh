#!/bin/bash
#-------------------------------------------------
# Shell script to update hubs.db locally to contain the 
# most recent Mobility data released from Google and Apple,
# as well as the latest COVID statistics collected by UofT
# Unlike update_data.sh, this is designed to run in one's 
# host machine, not in a docker image.
#
# update_data.py, and update_data.sql
# must be in ./docker/update_hubs/,
# and hubs.db should be the current working directory
#
# (C) Statistics Canada
# Author: Zachary Zanussi, Andres Solis Montero
#-------------------------------------------------

cd ./docker/update_hubs/

./update_data.sh ../../data/hubs.db 

