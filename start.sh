#!/bin/bash
#-------------------------------------------------
# Shell script to run the ASGI wrapper for the hubs.db
#
# (C) Statistics Canada
# Author: Andres Solis Montero
#-------------------------------------------------

if [ -z ${JUPYTER_SERVER_URL} ]; 
then
     datasette data/hubs.db --cors --config max_returned_rows:100000 --config sql_time_limit_ms:5500 &
else 
     export BASE_URL="https://kubeflow.covid.cloud.statcan.ca${JUPYTER_SERVER_URL:19}proxy/8001/"
     echo "Base url: ${BASE_URL}"
     datasette data/hubs.db --cors --config max_returned_rows:100000 --config sql_time_limit_ms:5500 --config base_url:${BASE_URL} &
fi

PIDS[0]=$!
./app.py &
PIDS[1]=$!

trap "kill ${PIDS[*]}" SIGINT
wait
