#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# ----------------------------------------------------------------------------------------------
# This script is to generate High Risk pandemic hubs labels from our sqlite database
#
# (C) 2020 Statistics Canada
# Authors: Andres Solis Montero
# ----------------------------------------------------------------------------------------------

import sqlite3
import pandas as pd
import numpy as np
from collections import Counter
from tqdm import tqdm 
import argparse

keys   = ['cases', 'cumulative_cases', 'deaths', 'cumulative_deaths']

def create_labels(dfs, top, normalize=False, default=None, lookback=True):
    columns = {}
    labels     = {}
    
    for key in tqdm(keys):
        #get all the columns names except the report_date
        cols = dfs[key].columns[1:].tolist()

        labels[key] = dfs[key].copy()
        labels[key] = labels[key][cols].astype('float')

        #order from max to min of the values per day. 
        order        = np.argsort(-dfs[key][cols].values, axis=1)
        #get the column index given the order
        rows         = dfs[key][cols].columns[order]
        #access the indices 
        index        = dfs[key][cols].index 

        #only display health regions that are among the max values
        _display_hruid = []


        #iterate over the sorted rows
        for _i, row in enumerate(rows):
            #max_value represent the maximum value for day at index _i
            max_value = labels[key].at[index[_i], row[0]]
            #iterate over the cols of a sorted row
            for _j, col in enumerate(row):
                
                value = labels[key].at[index[_i], col]
                #If it's not a max value, make it zero (no high-risk region)
                if _j > (top - 1):
                    ## if the HR was flagged the previous day as a high Risk
                    # and the number of cases/cum cases has increased, then continue 
                    # its state of high risk (only if lookback is True)
                    # otherwise make all values zero. 
                    
                    value = 0
                    if lookback and \
                         _i > 0 and \
                        labels[key].at[index[_i-1], col] > 0 and \
                        dfs[key].at[index[_i], col] > dfs[key].at[index[_i-1], col]:
                                
                        if normalize and max_value > 0 :
                            value /= float(max_value)
                        elif default is not None and default == 0:
                            value = labels[key].at[index[_i-1], col]
                        elif default > 0:
                            value = default
                
                else:
                #if it's among the max values display this hr_uid
                    _display_hruid.append(col)

                    if normalize and max_value > 0 :
                        value /= float(max_value)
                    elif default is not None and default == 0:
                        value = _j + 1
                    elif default > 0:
                        value = default
                
                labels[key].at[index[_i], col] = value

        counter       = Counter(_display_hruid).most_common(100)
        columns[key]  = [hr_uid for hr_uid, count in counter if count > 0] 
    return labels, columns


def create_df_day_hr(query, cnx):
    df3 = pd.read_sql_query(query, cnx)
    
    data   = {k: {'report_date':[]} for k in keys}

    for date, hr_df in tqdm(df3.groupby('report_date')):
        for k in keys:
            data[k]['report_date'].append(date) 

        for _, row in hr_df.iterrows():
            uid = row.HR_UID
            for k in keys:
                value = row[k]
                if uid in data[k]:
                    data[k][uid].append(value)
                else:
                    data[k][uid] = [value]

    # The variable dfs will hold a dictionary of the 
    # DataFrames, the keys to acccess the DataFrames inside dfs are 
    # contained in the variable keys

    dfs    = {}
    for k in keys:
        df = pd.DataFrame (data[k], columns = data[k].keys())
        df.set_index(pd.DatetimeIndex(df['report_date']), inplace=True)
        dfs[k]   = df
    return dfs 

def save_to_sqlite(cnx, labels, name='HIGH_RISK_', top=6):
    output = pd.DataFrame(columns=['report_date', 
                                   'HR_UID', 
                                   f'cases',
                                   f'cumulative_cases',
                                   f'deaths',
                                   f'cumulative_deaths'])

    cdf = labels['cases']
    ccdf= labels['cumulative_cases']
    ddf = labels['deaths']
    cddf= labels['cumulative_deaths']
    
    count = 0
    for i in tqdm(cdf.index):
        for j in cdf.columns:
            _i_str = str(i).split()[0]
            output.loc[count]= [_i_str, j, 
                                cdf.at[i, j], 
                                ccdf.at[i, j], 
                                ddf.at[i, j], 
                                cddf.at[i, j]]
            count+=1
    output.to_sql(f'{name}{top}', con=cnx, index=False)
    return output 



def esri_data(cnx):
    esri_df = pd.read_csv("https://opendata.arcgis.com/datasets/3aa9f7b1428642998fa399c57dad8045_1.csv")
    esri_df[['report_date', 'report_time']] = esri_df['Last_Updated'].str.split(' ', n=1, expand=True)
    esri_hr_date = esri_df[['report_date', 'HR_UID', 'CaseCount', 'Deaths', 'Recovered', 'Tests']]
    esri_hr_date = esri_hr_date.sort_values(by=['HR_UID', 'report_date'])
    esri_hr_date = esri_hr_date.rename(columns={
            "CaseCount":"cumulative_cases",
            "Deaths": "cumulative_deaths",
            "Recovered": "cumulative_recovered",
            "Tests":"cumulative_tests"
        })
    esri_hr_date['report_date'] = esri_hr_date['report_date'].str.replace('/','-')
    esri_hr_date = esri_hr_date.fillna(0)
    rename = {471: [4711,4712,4713],
          472: [4710, 4709,4708], 
          473: [4707, 4705],
          474: [4706],
          475: [4704],
          476: [4701, 4702, 4703], 
          591: [5911, 5912, 5913, 5914],
          592: [5921, 5922, 5923],
          593: [5931, 5932, 5933],
          594: [5941, 5942, 5943],
          595: [5951, 5952, 5953]
    }
    print(f'Translating Health Region Codes')
    for k, values in tqdm(rename.items()):
        tmp     = esri_hr_date[esri_hr_date.HR_UID==k]
        for v in values:
            tmp['HR_UID'] = v
            esri_hr_date = esri_hr_date.append(tmp, ignore_index=True)
        esri_hr_date.drop(esri_hr_date[esri_hr_date.HR_UID==k].index, inplace=True)
    
    print(f'Computing daily cases')
    esri_hr_date['cases']    = esri_hr_date['cumulative_cases']
    esri_hr_date['deaths']   = esri_hr_date['cumulative_deaths']
    esri_hr_date['recovered']= esri_hr_date['cumulative_recovered']
    esri_hr_date['tests']    = esri_hr_date['cumulative_tests']


    hrs = sorted(set(esri_hr_date['HR_UID'].to_list()))
    labels = ['cases','deaths','recovered','tests']
    for hr in hrs:
        for l in labels:
            cum  = esri_hr_date[f"cumulative_{l}"][esri_hr_date.HR_UID==hr]
            fill_value = cum.values[0]
            prev = cum.shift(periods=1,fill_value=fill_value)
            esri_hr_date[f"{l}"][esri_hr_date.HR_UID==hr] = cum - prev 



    esri_table = 'COVID19_ESRI'
    cnx.execute(f'DROP TABLE IF EXISTS {esri_table}')
    esri_hr_date.to_sql(f'{esri_table}', con=cnx, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("db", help="path to sqlite3 database")
    parser.add_argument("preffix", help="table name preffix", default="HIGH_RISK_" )
    parser.add_argument("top", help="minimum number of top labels", default=6, type=int)
    args = parser.parse_args()

    query = """
      SELECT
                  hr.PRUID, 
                  P.NAME,
                  hr.HR_UID, hr.ENGNAME, 
                  hr.report_date, 
                  (100000.0 * hr.cases            / HI.Population_rate) as cases,
                  (100000.0 * hr.cumulative_cases / HI.Population_rate) as cumulative_cases,
                  (100000.0 * hr.deaths           / HI.Population_rate) as deaths,
                  (100000.0 * hr.cumulative_deaths/ HI.Population_rate) as cumulative_deaths
      FROM 
           Covid19_HR_V as hr, Provinces as P, Health_Indicators_ODHF as HI
      WHERE 
           P.PRUID = hr.PRUID AND hr.HR_UID = HI.HR_UID 
      ORDER BY
           hr.report_date
         
    """
    cnx = sqlite3.connect(args.db)
    cnx.execute(f'DROP TABLE IF EXISTS {args.preffix}{args.top}')
    print(f'DROP TABLE IF EXISTS {args.preffix}{args.top}')
    ndfs = create_df_day_hr(query, cnx)
    print('Generating Daily Covid Data')
    labels, columns = create_labels(ndfs,args.top, normalize=False, default=1, lookback=True)
    print(f'Saving Labels for TOP {args.top}  in {args.preffix}{args.top}')
    save_to_sqlite(cnx, labels, name=args.preffix, top=args.top)
    print(f'Completed')

    print(f'Retrieving ESRI Covid19 Data')
    esri_data(cnx)
    print(f'Finished')