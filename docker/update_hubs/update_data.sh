#!/bin/bash
#-------------------------------------------------
# Shell script to update hubs.db to contain the 
# most recent Mobility data released from Google and Apple,
# as well as the latest COVID statistics collected by UofT
# This should only be run in the docker image!
#
# update_data.py, and update_data.sql
# must be in the same directory as this script to run,
# and hubs.db should be two directories up
#
# (C) Statistics Canada
# Author: Zachary Zanussi
#-------------------------------------------------


./update_data.py "$(< apple.txt)"

sqlite3 $1 < ./update_data.sql

./create_labels.py $1 HIGH_RISK_ 6
./create_labels.py $1 HIGH_RISK_ 10
./create_labels.py $1 HIGH_RISK_ 15
./create_labels.py $1 HIGH_RISK_ 20

rm apple_data.csv \
	case_hr_tr.csv \
	Covid19.csv \
	Global_Google_Mobility.csv \
	mortality_hr_tr.csv \
	google_interm.csv \
	country_interm.csv \
	apple_interm.csv \
	apple_cities.csv \
	combined_provs.csv \
	combined_country.csv \
	combined_city.csv \

