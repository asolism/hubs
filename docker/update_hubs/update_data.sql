# ------------------------------------------
# SQL script to update hubs.db
# Run in a directory containing hubs.db and the 
# results of running update_data.py
#
# (C) Statistics Canada
# Authors: Zachary Zanussi and Denise Chen
# ------------------------------------------
#.shell pwd
#################### Zac's Code #############

DROP TABLE IF EXISTS Mobility_Cities;
DROP TABLE IF EXISTS Mobility_Provinces;
DROP TABLE IF EXISTS Mobility_Canada;

##### REMOVE OLD TABLES, IF THEY EXIST ######
DROP TABLE IF EXISTS Google_Mobility;
DROP VIEW IF EXISTS Google_Mobility_v;
DROP TABLE IF EXISTS Apple_Mobility;
DROP VIEW IF EXISTS Apple_Cities_v;
DROP VIEW IF EXISTS Apple_Provinces_v;
DROP TABLE IF EXISTS Mobility;
DROP VIEW IF EXISTS Mobility_v;
DROP TABLE IF EXISTS Covid19_HR;
DROP TABLE IF EXISTS Covid19_TR;
#############################################

.mode csv


CREATE TABLE [Mobility_Cities] ( 
  [date] TEXT,
  [name] TEXT,
  [HR_UID] TEXT,
  [driving] NUMERIC,
  [transit] NUMERIC,
  [walking] NUMERIC
);

CREATE TABLE [Mobility_Provinces] (
  [province] TEXT,
  [PRUID] TEXT,
  [date] TEXT,
  [driving] NUMERIC,
  [retail_and_recreation] NUMERIC,
  [grocery_and_pharmacy] NUMERIC,
  [parks] NUMERIC,
  [transit_stations] NUMERIC,
  [workplaces] NUMERIC,
  [residential] NUMERIC
);

CREATE TABLE [Mobility_Canada] (
  [date] TEXT,
  [name] TEXT,
  [HR_UID] TEXT,
  [driving] NUMERIC, 
  [transit] NUMERIC,
  [walking] NUMERIC,
  [retail_and_recreation] NUMERIC,
  [grocery_and_pharmacy] NUMERIC,
  [parks] NUMERIC,
  [transit_stations] NUMERIC,
  [workplaces] NUMERIC,
  [residential] NUMERIC
);

.import ./combined_provs.csv Mobility_Provinces
.import ./combined_country.csv Mobility_Canada
.import ./combined_city.csv Mobility_Cities

-- CREATE VIEW Google_Mobility_v AS
-- select
--   HR.HR_UID, HR.ENGNAME as HR_Name, MO.sub_region_1 as Province, MO.date, MO.retail_and_recreation, MO.grocery_and_pharmacy, MO.parks, MO.transit_stations, MO.workplaces, MO.residential
-- from
--   HR
-- inner join Google_Mobility as MO on MO.PRUID = HR.PRUID
-- order by HR.HR_UID;

-- CREATE VIEW Apple_Provinces_v as 
-- select name, code as PRUID, transportation, date, percent from Apple_Mobility 
-- where type = "province"
-- order by name;

-- CREATE VIEW Apple_Cities_v as
-- select name, code as HR_UID, transportation, date, percent from Apple_Mobility
-- where type = "city"
-- order by name;

####### Denise's Code: ###########
####### Modified: Andres Solis Montero 2020-05-28 ###########


DROP TABLE IF EXISTS Covid19;

.mode csv

CREATE TABLE [Covid19] (
  [PRUID] TEXT,
  [HR_UID] TEXT,
  [health_region] TEXT,
  [report_date] DATE,
  [cases] INTEGER,
  [cumulative_cases] INTEGER,
  [deaths] INTEGER,
  [cumulative_deaths] INTEGER
);

.import ./Covid19.csv Covid19


####### Andres Solis Montero Code:  ###########
####### Covid19 Views              ###########

DROP VIEW IF EXISTS Covid19_Prov_V;
CREATE VIEW Covid19_Prov_V AS 
SELECT 
 H.PRUID,
 P.NAME,
 report_date, 
 sum(cases) as cases,
 sum(cumulative_cases) as cumulative_cases, 
 sum(deaths) as deaths,
 sum(cumulative_deaths) as cumulative_deaths
FROM (SELECT 
 c.PRUID,
 c.HR_UID,
 h.ENGNAME,
 c.report_date,
 sum(c.cases) as cases,
 sum(c.cumulative_cases) as cumulative_cases,
 sum(c.deaths) as deaths,
 sum(c.cumulative_deaths) as cumulative_deaths
FROM
 Covid19 as c 
LEFT JOIN HR as h ON c.HR_UID = h.HR_UID
GROUP BY c.HR_UID, c.report_date
ORDER BY c.HR_UID, c.report_date) as H, Provinces as P
WHERE H.PRUID = P.PRUID
GROUP BY H.PRUID, report_date
ORDER BY H.PRUID, report_date;


DROP VIEW IF EXISTS Covid19_HR_V;
CREATE VIEW Covid19_HR_V AS 
SELECT 
 c.PRUID,
 c.HR_UID,
 h.ENGNAME,
 c.report_date,
 sum(c.cases) as cases,
 sum(c.cumulative_cases) as cumulative_cases,
 sum(c.deaths) as deaths,
 sum(c.cumulative_deaths) as cumulative_deaths
FROM
 Covid19 as c 
LEFT JOIN HR as h ON c.HR_UID = h.HR_UID
GROUP BY c.HR_UID, c.report_date
ORDER BY c.HR_UID, c.report_date;


DROP VIEW IF EXISTS Covid19_Nacional_V;
CREATE VIEW Covid19_Nacional_V AS 
SELECT 
   report_date, 
   sum(cases) as cases, 
   sum(cumulative_cases) as cumulative_cases,
   sum(deaths) as deaths,
   sum(cumulative_deaths) as cumulative_deaths
FROM 
   Covid19  
GROUP BY 
   report_date;



####### Geometry Views              ###########

DROP VIEW IF EXISTS HR_V;

CREATE VIEW  HR_V AS 
select 
  h.PRUID, 
  h.HR_UID, 
  c.updated, 
  c.cases, 
  c.deaths, 
  h.ENGNAME, 
  h.geometry from HR as h
LEFT JOIN 
  (select 
      PRUID, 
      HR_UID, 
      max(report_date) as updated , 
      sum(cases) as cases, 
      sum(deaths) as deaths 
      from Covid19_HR_V 
      group by HR_UID) as c 
ON h.HR_UID = c.HR_UID;

DROP VIEW IF EXISTS DA_V;
CREATE VIEW DA_V AS 
    SELECT
        HR.HR_UID, HR.ENGNAME, HR.FRENAME, DA.PRUID, DA.geometry 
    FROM
        DA, HR 
    WHERE DA.HR_UID == HR.HR_UID;


####### Mobility Views             ###########
  
DROP VIEW IF EXISTS Mobility_HR_V;

CREATE VIEW Mobility_HR_V as 
SELECT 
      HRex.PRUID   as PRUID, 
      HRex.HR_UID  as HR_UID, 
      M.province   as PROV, 
      HRex.ENGNAME as HR_NAME, 
      M.date       as DATE,
      /* Using Population_Density (number of ppl / km2) */
      M.driving               * HRex.Population_density as Md_PD, 
      M.retail_and_recreation * HRex.Population_density as Mrr_PD,
      M.grocery_and_pharmacy  * HRex.Population_density as Mgp_PD,
      M.parks                 * HRex.Population_density as Mp_PD,
      M.transit_stations      * HRex.Population_density as Mt_PD,
      M.workplaces            * HRex.Population_density as Mw_PD,
      M.residential           * HRex.Population_density as Mr_PD,
     (  M.driving               * HRex.Population_density  +  
        M.retail_and_recreation * HRex.Population_density  + 
        M.grocery_and_pharmacy  * HRex.Population_density  + 
        M.parks                 * HRex.Population_density  + 
        M.transit_stations      * HRex.Population_density  + 
        M.workplaces            * HRex.Population_density  + 
        M.residential           * HRex.Population_density   ) / 7.0 as M_PD, 
 /* Using CMA_CA_Density (number of ppl / km2) */
      M.driving               * HRex.CMA_CA_density as Md_MD, 
      M.retail_and_recreation * HRex.CMA_CA_density as Mrr_MD,
      M.grocery_and_pharmacy  * HRex.CMA_CA_density as Mgp_MD,
      M.parks                 * HRex.CMA_CA_density as Mp_MD,
      M.transit_stations      * HRex.CMA_CA_density as Mt_MD,
      M.workplaces            * HRex.CMA_CA_density as Mw_MD,
      M.residential           * HRex.CMA_CA_density as Mr_MD,
     (  M.driving               * HRex.CMA_CA_density  +  
        M.retail_and_recreation * HRex.CMA_CA_density  + 
        M.grocery_and_pharmacy  * HRex.CMA_CA_density  + 
        M.parks                 * HRex.CMA_CA_density  + 
        M.transit_stations      * HRex.CMA_CA_density  + 
        M.workplaces            * HRex.CMA_CA_density  + 
        M.residential           * HRex.CMA_CA_density   ) / 7.0 as M_MD, 
        /* Using 	Propo_Population_Rural  ( ppl in rural area / total pp in HR) */
        M.driving               * HRex.Propo_Population_Rural as Md_RP, 
        M.retail_and_recreation * HRex.Propo_Population_Rural as Mrr_RP,
        M.grocery_and_pharmacy  * HRex.Propo_Population_Rural as Mgp_RP,
        M.parks                 * HRex.Propo_Population_Rural as Mp_RP,
        M.transit_stations      * HRex.Propo_Population_Rural as Mt_RP,
        M.workplaces            * HRex.Propo_Population_Rural as Mw_RP,
        M.residential           * HRex.Propo_Population_Rural as Mr_RP,
        (   M.driving               * HRex.Propo_Population_Rural  +  
            M.retail_and_recreation * HRex.Propo_Population_Rural  + 
            M.grocery_and_pharmacy  * HRex.Propo_Population_Rural  + 
            M.parks                 * HRex.Propo_Population_Rural  + 
            M.transit_stations      * HRex.Propo_Population_Rural  + 
            M.workplaces            * HRex.Propo_Population_Rural  + 
            M.residential           * HRex.Propo_Population_Rural   ) / 7.0 as M_RP,
      /* Using 	Propo_Population_CA_CMA  ( ppl in metrop area / total pp in HR) */
      M.driving               * HRex.Propo_Population_CA_CMA as Md_MP, 
      M.retail_and_recreation * HRex.Propo_Population_CA_CMA as Mrr_MP,
      M.grocery_and_pharmacy  * HRex.Propo_Population_CA_CMA as Mgp_MP,
      M.parks                 * HRex.Propo_Population_CA_CMA as Mp_MP,
      M.transit_stations      * HRex.Propo_Population_CA_CMA as Mt_MP,
      M.workplaces            * HRex.Propo_Population_CA_CMA as Mw_MP,
      M.residential           * HRex.Propo_Population_CA_CMA as Mr_MP,
      (   M.driving               * HRex.Propo_Population_CA_CMA  +  
          M.retail_and_recreation * HRex.Propo_Population_CA_CMA  + 
          M.grocery_and_pharmacy  * HRex.Propo_Population_CA_CMA  + 
          M.parks                 * HRex.Propo_Population_CA_CMA  + 
          M.transit_stations      * HRex.Propo_Population_CA_CMA  + 
          M.workplaces            * HRex.Propo_Population_CA_CMA  + 
          M.residential           * HRex.Propo_Population_CA_CMA   ) / 7.0 as M_MP  
FROM (
    SELECT  
          HR.PRUID, 
          HR.HR_UID, 
          HR.ENGNAME, 
          HI.Population_density, 
          HI.Propo_Population_Rural, 
          HI.Propo_Population_CA_CMA,
          HI.CMA_CA_density
    FROM 
          HR, Health_Indicators_ODHF as HI 
    WHERE 
          HR.HR_UID = HI.HR_UID ) as HRex
JOIN 
Mobility_Provinces as M
ON HRex.PRUID = M.PRUID;


DROP VIEW IF EXISTS Mobility_Prov_V;
CREATE VIEW Mobility_Prov_V as 
SELECT 
m.PRUID,
p.NAME,
m.date, 

 /* Using Population_Density (number of ppl / km2) */
m.driving               * p.Population_density as Md_PD,
m.retail_and_recreation * p.Population_density as Mrr_PD,
m.grocery_and_pharmacy  * p.Population_density as Mgp_PD,
m.parks                 * p.Population_density as Mp_PD, 
m.transit_stations      * p.Population_density as Mt_PD, 
m.workplaces            * p.Population_density as Mw_PD,
m.residential           * p.Population_density as Mr_PD,
(m.driving + 
 m.retail_and_recreation + 
 m.grocery_and_pharmacy + 
 m.parks + 
 m.transit_stations + 
 m.workplaces + 
 m.residential) * p.Population_density / 7.0 as M_PD,
 /* Using CMA_CA_Density (number of ppl / km2) */
m.driving               * p.CA_CMA_density as Md_MD,
m.retail_and_recreation * p.CA_CMA_density as Mrr_MD,
m.grocery_and_pharmacy  * p.CA_CMA_density as Mgp_MD,
m.parks                 * p.CA_CMA_density as Mp_MD, 
m.transit_stations      * p.CA_CMA_density as Mt_MD, 
m.workplaces            * p.CA_CMA_density as Mw_MD,
m.residential           * p.CA_CMA_density as Mr_MD,

(m.driving + 
 m.retail_and_recreation + 
 m.grocery_and_pharmacy + 
 m.parks + 
 m.transit_stations + 
 m.workplaces + 
 m.residential) * p.Population_density / 7.0 as M_PD,
/* Using 	Propo_Population_Rural  ( ppl in rural area / total pp in HR) */
m.driving               * (p.Population_Rural/p.Population) as Md_RP,
m.retail_and_recreation * (p.Population_Rural/p.Population) as Mrr_RP,
m.grocery_and_pharmacy  * (p.Population_Rural/p.Population) as Mgp_RP,
m.parks                 * (p.Population_Rural/p.Population) as Mp_RP, 
m.transit_stations      * (p.Population_Rural/p.Population) as Mt_RP, 
m.workplaces            * (p.Population_Rural/p.Population) as Mw_RP,
m.residential           * (p.Population_Rural/p.Population) as Mr_RP,

(m.driving + 
 m.retail_and_recreation + 
 m.grocery_and_pharmacy + 
 m.parks + 
 m.transit_stations + 
 m.workplaces + 
 m.residential) * (p.Population_Rural/p.Population) / 7.0 as M_RP,
/* Using 	Propo_Population_CA_CMA  ( ppl in metrop area / total pp in HR) */
m.driving               * (p.Population_CA_CMA/p.Population) as Md_MP,
m.retail_and_recreation * (p.Population_CA_CMA/p.Population) as Mrr_MP,
m.grocery_and_pharmacy  * (p.Population_CA_CMA/p.Population) as Mgp_MP,
m.parks                 * (p.Population_CA_CMA/p.Population) as Mp_MP, 
m.transit_stations      * (p.Population_CA_CMA/p.Population) as Mt_MP, 
m.workplaces            * (p.Population_CA_CMA/p.Population) as Mw_MP,
m.residential           * (p.Population_CA_CMA/p.Population) as Mr_MP,

(m.driving + 
 m.retail_and_recreation + 
 m.grocery_and_pharmacy + 
 m.parks + 
 m.transit_stations + 
 m.workplaces + 
 m.residential) * (p.Population_CA_CMA/p.Population) / 7.0 as M_MP

FROM
Mobility_Provinces m 
LEFT JOIN Provinces as p 
ON p.PRUID = m.PRUID;