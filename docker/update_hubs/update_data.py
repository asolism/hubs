#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# ----------------------------------------------------------------------------------------------
# This script is designed to download the latest Google Mobility Dataset and Apple Mobility, 
# retain only the data  relevant to Canada, and do some minor preprocessing
# to make the data a little cleaner. 
# 
# Also, we remove the column labels from the final product for loading into hubs.db
#
# The Google data has data for every day from every province and territory and the 
# nation as a whole, while the Apple data has data for every day from every province
# and territory, 7 cities, and the nation.
#
# Then, we run Denise's code, which gets the newest version of the COVID stats from the
# UofT database. There is data for many health regions and it is daily.
#
# (C) 2020 Statistics Canada
# Authors: Zachary Zanussi and Denise Chen
# ----------------------------------------------------------------------------------------------
# Updated 2020-05-29 by Zachary Zanussi to combine and rearrange the Apple and Google mobility data

# Import Dependencies
import urllib.request as url
import csv
import pandas as pd
import sys


directory = './'

print("Downloading Google data... ", end = '')
url_google = "https://www.gstatic.com/covid19/mobility/Global_Mobility_Report.csv?cachebust=e0c5a582159f5662"
file_google = directory + 'Global_Google_Mobility.csv' # Where we will download the original data to
url.urlretrieve(url_google,file_google)
print('Done!')


print('Downloading Apple data... ', end = '')
if len(sys.argv) > 1:
    url_apple = sys.argv[1]
else:
    print("No Apple URL provided; defaulting to 2020-05-27")
    url_apple = "https://covid19-static.cdn-apple.com/covid19-mobility-data/2009HotfixDev16/v3/en-us/applemobilitytrends-2020-06-01.csv"
#url_apple = "https://covid19-static.cdn-apple.com/covid19-mobility-data/2007HotfixDev54/v2/en-us/applemobilitytrends-2020-05-09.csv"
file_apple = directory + "apple_data.csv"
url.urlretrieve(url_apple, file_apple)
print('Done!')


######## Dictionaries, helpers, etc ################################
prov_dict = {'':'00', "Canada":"00", "Newfoundland and Labrador":"10", "Prince Edward Island":"11", "Nova Scotia":"12", "New Brunswick":"13",
             "Quebec":"24", "Ontario":"35", "Manitoba":"46", "Saskatchewan":"47", "Alberta":"48", "British Columbia":"59", 
             "Yukon":"60", "Northwest Territories":"61", "Nunavut":"62", 'Calgary':'4832', 'Edmonton':'4834',
            'Vancouver':'5932', 'Halifax':'1204', 'Toronto':'3595', 'Ottawa':'3551', 'Montreal':'2406'}

canadian_cities = ['Calgary','Edmonton','Halifax','Montreal','Ottawa','Toronto','Vancouver']

prov_terr = ['Alberta', 'British Columbia', 'Manitoba', 'New Brunswick',
             'Newfoundland and Labrador', 'Northwest Territories',
           'Nova Scotia', 'Nunavut', 'Ontario', 'Prince Edward Island',
           'Quebec', 'Saskatchewan', 'Yukon Territory']           

def simplify_colnames(colnames): # Removes _percent_change_from_baseline from every column name
    cols = []
    for col in colnames:
        if col.endswith("_percent_change_from_baseline"):
            cols.append(col[:-29])
        else:
            cols.append(col)
    return cols

def sub100(val): # Subtracts 100 from the apple data with some error handling
    if not val:
        return val
    return float(val) - 100

def comb(x): # Helper function for groupby in the cities
    newf = x.pivot(index = 'date', columns = 'transportation')
    newf.columns = newf.columns.map(''.join)
    newf = newf.reset_index(level=0)[['date','typedriving','namedriving','HR_UIDdriving','percentdriving','percenttransit','percentwalking']]
    newf.columns = ['date','type','name','HR_UID','driving','transit','walking']
    return newf

def interpolate_data_series(series):
    #### Fill in zeros to the front
    if series[0] != series[0]:
        series[0] = 0
        i = 1
        #### Code to insert 0's in the beginning
#         while series[i] != series[i] and i < len(series):
#             series[i] = 0
#             i = i + 1
#             if i >= len(series):
#                 return pd.Series([0] * len(series))
        ### Code to linearly interpolate in the beginning
        while series[i] != series[i]:
            i = i + 1
            if i >= len(series):
                return pd.Series([0] * len(series))
        last_val = series[i]
        for k in range(i):
            series[k] = last_val * k / i
            
    #### Fill in the end with the last value
    i = len(series) - 1
    while series[i] != series[i]:
        i = i - 1
    fill_val = series[i]
    while i < len(series):
        series[i] = fill_val
        i = i + 1
            
    #### Interpolate linearly between values
    i = 1
    while i < len(series): # Search the entire column
        if series[i] != series[i]: # Find a missing value, record it,
            first_val = series[i - 1]
            j = 1
            while series[i + j] != series[i + j]: # and search for the next filled value, record it
                j = j + 1
            last_val = series[i + j]
            for k in range(i, i + j): # Linearly interpolate between the values
                series[k] = first_val + (last_val - first_val) * (k - i + 1) / (j + 1)        
                
        i = i + 1
    

    return series
    
def interpolate_data(frame, num_cols):
    frame = frame.reset_index(drop = True)
    frame[frame.columns[-num_cols:]] = frame[frame.columns[-num_cols:]].transform(interpolate_data_series, axis = 0) / 100
    return frame

##############################################################################################



######################### Initial separating into several csv's ##############################

# This contains the google provincial data
g_interm = directory + 'google_interm.csv'
# This contains the google national data
country_interm = directory + 'country_interm.csv'
# This contains the apple provincial data
a_interm = directory + 'apple_interm.csv'
# This contains the apple cities and national data
cityfile = directory + 'apple_cities.csv'

# Output files for each of the three levels
prov_output = directory + 'combined_provs.csv'
nat_output  = directory + 'combined_country.csv'
city_output = directory + 'combined_city.csv'


# This code takes the raw csv's and converts them into 4 smaller intermediate csv's
print('Separating... ', end = '')
with open(file_google, 'r') as gfile, open(file_apple, 'r') as afile, open(a_interm, 'w') as ap, open(g_interm, 'w') as goog, open(cityfile, 'w') as cities, open(country_interm,'w') as g_country:
    google_reader = csv.reader(gfile)
    apple_reader = csv.reader(afile)
    
    # This contains the apple provincial data
    ap_writer = csv.writer(ap)
    # This contains the google provincial data
    goog_writer = csv.writer(goog)
    # This contains apple city and national data 
    city_writer = csv.writer(cities)
    # This contains the google national data
    country_writer = csv.writer(g_country)
    
    g_colnames = google_reader.__next__()
    a_colnames = apple_reader.__next__()
    
    ## Write the headers for each of the intermediate csv's  
    goog_writer.writerow(simplify_colnames( ['province',"PRUID"] + g_colnames[6:]))
    country_writer.writerow(simplify_colnames( ['name', "HR_UID"] + g_colnames[6:]))
    ap_writer.writerow(['province','PRUID','date','driving'])
    city_writer.writerow(["type",'name','HR_UID','transportation','date','percent'])
    
    
    ### Google Data
    for row in google_reader:
        if row[0] == 'CA':
            pruid = prov_dict[row[2]]
            if pruid == "00":
                country_writer.writerow(row[1:2] + [pruid] + row[6:])
            else:
                goog_writer.writerow(row[2:3] + [pruid] + row[6:])
                
    ### Apple Data
    dates = a_colnames[6:]
    
    for row in apple_reader:
        if row[1] in canadian_cities + ["Canada"]:
            for i in range(len(dates)):
                if row[0] == 'country/region':
                    row[0] = 'country'
                city_writer.writerow(row[0:2] + [prov_dict[row[1]]] + row[2:3] + [dates[i]] + [sub100(row[6 + i])])
        if row[1] in prov_terr:
            for i in range(len(dates)):
                if row[1] == 'Yukon Territory':
                    row[1] = 'Yukon'
                ap_writer.writerow(row[1:2] + [prov_dict[row[1]]] + [dates[i]] + [sub100(row[6 + i])])

print('Done!')

# Merge using pandas and output for adding to the database
# Provincial
gdf = pd.read_csv(g_interm)
adf = pd.read_csv(a_interm)
provs = adf.merge(gdf, on=['date','province','PRUID'],how = 'outer', sort=False)
# provs[['driving', 'retail_and_recreation','grocery_and_pharmacy', 'parks', 'transit_stations', 'workplaces','residential']] \
#         = provs[['driving', 'retail_and_recreation','grocery_and_pharmacy', 'parks', 'transit_stations', 'workplaces',
#        'residential']].transform(interpolate_data, axis = 0) / 100
provs = provs.groupby('province', sort = False).apply(lambda x: interpolate_data(x,7))
provs.index = range(provs.shape[0])
provs.to_csv(prov_output, float_format = '%.5f', header = False, index = False)

# National
gcountry = pd.read_csv(country_interm, usecols=["date","name",'HR_UID',"retail_and_recreation", 'grocery_and_pharmacy','parks','transit_stations','workplaces','residential'], dtype={'HR_UID':'object'})
cdf = pd.read_csv(cityfile, dtype={'HR_UID':'object'})
acountry = comb(cdf[cdf["name"] == 'Canada']).drop(columns = 'type')
#print(gcountry.head())
#print(acountry.head())
country_frame = acountry.merge(gcountry, on= ['name','date', 'HR_UID'], how='outer')
country_frame[['driving','transit','walking','retail_and_recreation', 'grocery_and_pharmacy', 'parks','transit_stations', 'workplaces', 'residential']] \
        = country_frame[['driving','transit','walking','retail_and_recreation', 'grocery_and_pharmacy', 'parks',
       'transit_stations', 'workplaces', 'residential']].transform(interpolate_data_series, axis = 0) / 100
country_frame.to_csv(nat_output, float_format = '%.5f', header = False, index = False)

# City
by_name = cdf[cdf['name'] != 'Canada'].groupby('name', sort=False).apply(comb).drop(columns='type')
by_name.index = range(by_name.shape[0])
by_name[['driving','transit','walking']] = by_name[['driving','transit','walking']].transform(interpolate_data_series, axis = 0) / 100
by_name.to_csv(city_output, float_format = '%.5f', header = False, index = False)
print('Saved national, provincial, and city data!')

##############################################################################################################
#                                                                                                            #  
#                                        DENISE'S CODE                                                       #
#                                                                                                            #
##############################################################################################################
# Modified 2020-05-28 by Andres Solis Montero 


import sqlite3

def download_data():
    # case time series
    url_to_load = "https://raw.githubusercontent.com/ishaberry/Covid19Canada/master/timeseries_hr/cases_timeseries_hr.csv"
    file_case = 'case_hr_tr.csv' # Where we will download the original data to
    url.urlretrieve(url_to_load,file_case)
    # mortality time series
    url_to_load = "https://raw.githubusercontent.com/ishaberry/Covid19Canada/master/timeseries_hr/mortality_timeseries_hr.csv"
    file_mortality = 'mortality_hr_tr.csv' # Where we will download the original data to
    url.urlretrieve(url_to_load,file_mortality)
    return file_case, file_mortality

def read(file_case, file_mortality):
    # read confirmed case into csv
    df_case = pd.read_csv(file_case)
    df_case["date_report"] = pd.to_datetime(df_case["date_report"], format='%d-%m-%Y')
    df_case.rename(columns={'date_report':'date'}, inplace=True)
    # read mortality case into csv
    df_mortality = pd.read_csv(file_mortality)
    df_mortality["date_death_report"] = pd.to_datetime(df_mortality["date_death_report"], format='%d-%m-%Y')
    df_mortality.rename(columns={'date_death_report':'date'}, inplace=True)
    # merge 2 datasets
    df_tr_total = pd.merge(df_case, df_mortality, how='left' , on=['province', 'health_region', 'date'])
    df_tr_total.fillna(0, inplace=True)
    df_tr_total['health_region'] = df_tr_total['health_region'].str.lower()
    # cols: province, hr, date, cases, cumulative_cases, deaths, cumulative_deaths
    return df_tr_total

def process(df_case_mort, output='Covid19.csv'):
    #Modified: Andres Solis Montero
    #         Removing redundancy naming of provinces and added direct reference to 
    #         Province and HR tables 
    df_process = df_case_mort.loc[(df_case_mort['health_region'] != 'not reported')]
    
    pr_dict = {'NL' : '10', 
               'PEI': '11', 
                'BC': '59',
               'NWT': '61', 
               'Alberta'      :'48', 
               'New Brunswick':'13',
               'Nova Scotia'  :'12', 
               'Ontario'      :'35', 
               'Quebec'       :'24', 
               'Saskatchewan' :'47',
               'Yukon'        :'60', 
               'Manitoba'     :'46', 
               "Nunavut"      :'62'}

    df_process['province'] = df_process['province'].map(pr_dict)
    # province becomes PRUID (Province ID)
    # reorder columns order into db
    df_db = pd.read_csv('hruid_name.csv')
    df_db['province'] = df_db.province.apply(str)
    df_db['HR_UID']   = df_db.HR_UID.apply(str)
    df_tr_db = pd.merge(df_process, df_db, how='left', on=['health_region','province'])
    df_tr_db = df_tr_db[['province', 'HR_UID', 'health_region', 'date', 'cases', 'cumulative_cases', 'deaths', 'cumulative_deaths']]
    # export dataframe into csv
    df_tr_db.to_csv(output, index=False, header = False)

if __name__ == "__main__":
    output = 'Covid19.csv'

    print('Downloading Covid19 data...')
    file_case, file_mortality = download_data()
    print('Formating ...')
    df_case_mort = read(file_case, file_mortality)
    print('Processing ...')
    process(df_case_mort, output)
    print(f'{output} file with Covid19 data xwas created created')
    # final csv with columns
    # PRUID, PR_name, health_region, report_date, cases, cumulative_cases, deaths, cumulative_deaths
