#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
# -----------------------------------------------------------------------------------
# (C) 2020 Statistics Canada
# Authors: Andres Solis Montero
# ----------------------------------------------------------------------------------
from os import environ
from flask import Flask, render_template, jsonify, request
import requests

application      = Flask(__name__)
datasette_url    = 'http://localhost:8001/hubs/HR_V.json'

@application.route("/")
def index():
    base_url = '5000/data' if "JUPYTER_SERVER_URL" in environ else datasette_url
    return render_template('index.html', title='Index', base_url=base_url)

@application.route("/data")
def contact():
    r = requests.get(datasette_url, params=request.args)
    return jsonify(r.json())

if __name__ == '__main__':
    application.run(debug=True)